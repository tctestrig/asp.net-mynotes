﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace System
{
    public static class DateTimeExtensions
    {
        private const string Today = "Today";
        private const string Tomorrow = "Tomorrow";

        public static bool IsOverdue(this DateTime dateTime)
        {
            return dateTime < DateTime.Now;
        }

        public static string GetDayString(this DateTime dateTime)
        {
            return dateTime.IsToday() ? Today : 
                dateTime.IsTomorrow() ? Tomorrow : 
                string.Empty;
        }

        private static bool IsToday(this DateTime dateTime)
        {
            return dateTime.Date == DateTime.Now.Date;
        }

        private static bool IsTomorrow(this DateTime dateTime)
        {
            return dateTime.Date == DateTime.Now.Date.AddDays(1.0d);
        }
    }
}

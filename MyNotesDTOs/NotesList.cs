﻿using MyNotesDTOs;
using MyNotesDTOs.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotesDTOs
{
    public class NotesList : List<Note>
    {
        public NotesList()
        { }

        public NotesList(List<Note> notes) : base(notes)
        { }

        public NotesList SortNotesByFinishDate(SortDirection sortDirection)
        {
            return GetSortedList(n => n.FinishDate, sortDirection);
        }

        public NotesList SortNotesByCreationDate(SortDirection sortDirection)
        {
            return GetSortedList(n => n.CreationDate, sortDirection);
        }

        public NotesList SortNotesByImportance(SortDirection sortDirection)
        {
            return GetSortedList(n => n.Importance, sortDirection);
        }

        public NotesList GetFinishedNotes()
        {
            return new NotesList(this.Where(a => a.IsFinished).ToList());
        }

        public NotesList GetUnfinishedNotes()
        {
            return new NotesList(this.Where(a => !a.IsFinished).ToList());
        }

        private NotesList GetSortedList<T>(Func<Note, T> selector, SortDirection sortDirection)
        {
            if (sortDirection == SortDirection.Ascending)
            {
                return new NotesList(this.OrderBy(selector).ToList());
            }
            else
            {
                return new NotesList(this.OrderByDescending(selector).ToList());
            }
        }
    }
}

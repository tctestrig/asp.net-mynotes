﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotesDTOs
{
    public class Note
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        [Range(0, 5)]
        public int Importance { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? CreationDate { get; set; }

        [DataType(DataType.DateTime)]
        [Required]
        public DateTime? FinishDate { get; set; }

        public bool IsFinished { get; set; }

        [NotMapped]
        public bool IsOverdue => FinishDate.HasValue && 
            !IsFinished && FinishDate.Value.IsOverdue();

        [NotMapped]
        public string DayString => FinishDate.HasValue ? 
            FinishDate.Value.GetDayString() : string.Empty;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotesDTOs.Enum
{
    public enum SortType { Undefined, FinishDate, CreatedDate, Importance }
}

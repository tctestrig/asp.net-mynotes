﻿using MyNotesDTOs;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNotesInterfaces
{
    public interface IRepository : IDisposable
    {
        Task<Note> CreateNewNoteAsync();
        void AddNote(Note note);
        Task AddNoteAsync(Note note);
        Task<Note> GetNoteByIdAsync(int id);
        void UpdateNote(Note note);
        void DeleteNote(Note note);
        bool NoteExists(int id);
        Task<NotesList> GetAllNotesAsync();
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
﻿using Microsoft.EntityFrameworkCore;
using MyNotesDatabase.Database;
using MyNotesDTOs;
using MyNotesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNotesDatabase
{
    public class Repository : IRepository
    {
        #region fields
        private readonly MyNotesDbContext _db;        
        #endregion

        public Repository(MyNotesDbContext db)
        {
            _db = db;
        }

        #region properties
        
        #endregion

        #region methods
        public async Task<Note> CreateNewNoteAsync()
        {
            Note note = new Note();
            await _db.Notes.AddAsync(note);
            return note;
        }

        public void AddNote(Note note)
        {
            _db.Notes.Add(note);
        }

        public async Task AddNoteAsync(Note note)
        {
            await _db.Notes.AddAsync(note);
        }

        public void UpdateNote(Note note)
        {
            _db.Update(note);
        }

        public void DeleteNote(Note note)
        {
            _db.Notes.Remove(note);
        }

        public async Task<Note> GetNoteByIdAsync(int id)
        {
            return await _db.Notes.FindAsync(id);
        }

        public async Task<NotesList> GetAllNotesAsync()
        {
            return new NotesList(await _db.Notes.ToListAsync());
        }

        public bool NoteExists(int id)
        {
            return _db.Notes.Any(a => a.Id == id);
        }

        public void SaveChanges()
        {
            SaveChangesAsync().Wait();
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                //todo
            }
            catch (DbUpdateException ex)
            {
                //todo
            }
        }

        public void Dispose()
        {
            _db.Dispose();
        }
        #endregion
    }
}

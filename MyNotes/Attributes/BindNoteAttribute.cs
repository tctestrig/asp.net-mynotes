﻿using Microsoft.AspNetCore.Mvc;
using MyNotes.ModelBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotes.Attributes
{
    public class BindNoteAttribute : ModelBinderAttribute
    {
        public BindNoteAttribute()
        {
            base.BinderType = typeof(NoteModelBinder);
        }
    }
}

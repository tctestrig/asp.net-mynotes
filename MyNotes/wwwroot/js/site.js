﻿function showFinished() {
    $.ajax({
        url: "/Notes/ShowFinished",
        type: "GET",
        success: function (result) {
            // refreshes view
            $('#bodyRegion').html(result);
        }
    });
}

function changeTheme(theme) {
    $.ajax({
        url: "/Notes/ChangeTheme",
        data: { theme: theme },
        type: "GET",
        success: function () {
            // reload page
            window.location.reload();
        }
    });
}

function sortByFinishDate() {
    callAction("SortByFinishDate");
}

function sortByCreationDate() {
    callAction("SortByCreationDate");
}

function sortByImportance() {
    callAction("SortByImportance");
}

function callAction(action) {
    $.ajax({
        url: "/Notes/" + action,
        data: {},
        type: "GET",
        success: function (result) {
            // refreshes view
            $('#bodyRegion').html(result);
        }
    });
}

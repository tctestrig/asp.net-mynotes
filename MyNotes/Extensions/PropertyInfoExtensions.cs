﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace System.Reflection
{
    public static class PropertyInfoExtensions
    {
        public static bool HasPublicSetter(this PropertyInfo property)
        {
            return property.GetSetMethod(true)?.IsPublic ?? default(bool);
        }
    }
}

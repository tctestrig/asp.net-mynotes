﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using MyNotesDatabase;
using MyNotesDTOs;
using MyNotesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Builder
{
    public static class ApplicationBuilderExtensions
    {
        private static readonly NotesList _notes = new NotesList
        {
            new Note
            {
                Title = "ASP.NET Core Projekt vorantreiben",
                Text = "Dieses Projekt bis Ende Sept. 2018 endlich mal fertigstellen :)",
                Importance = 5,
                CreationDate = new DateTime(2018, 01, 01),
                FinishDate = DateTime.Now.AddDays(1),
                IsFinished = false
            },
            new Note
            {
                Title = "Wieder einmal den Haushalt machen",
                Text = "Putzen und aufräumen",
                Importance = 1,
                CreationDate = new DateTime(2018, 06, 01),
                FinishDate = DateTime.Now.AddHours(1),
                IsFinished = true
            },
            new Note
            {
                Title = "Enikauf erledigen",
                Text = "Tomaten, Brötchen, Fleisch",
                Importance = 3,
                CreationDate = new DateTime(2018, 12, 01),
                FinishDate = DateTime.Now.AddDays(-1),
                IsFinished = false
            }
        };

        public static IApplicationBuilder AddDemoNotes(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            using (var repository = scope.ServiceProvider.GetRequiredService<IRepository>())
            {
                _notes.ForEach(n =>
                {
                    repository.AddNote(n);
                });
                repository.SaveChanges();
            }
            return app;
        }
    }
}

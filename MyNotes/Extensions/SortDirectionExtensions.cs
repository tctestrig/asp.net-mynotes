﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotesDTOs.Enum
{
    public static class SortDirectionExtensions
    {
        public static SortDirection ToggleDirection(this SortDirection sortDirection)
        {
            return sortDirection == SortDirection.Ascending ? 
                SortDirection.Descending : SortDirection.Ascending;
        }
    }
}

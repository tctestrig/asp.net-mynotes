﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationExtensions
    {
        private const string DatabaseNameKey = "InMemoryDatabase:DbName";

        public static string GetDatabaseName(this IConfiguration config)
        {
            return config.GetValue<string>(DatabaseNameKey);
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.Configuration
{
    public static class ConfigurationBuilderExtensions
    {
        private const string AppSettingsPath = "appsettings.json";

        public static IConfigurationBuilder AddConfigFiles(this IConfigurationBuilder builder)
        {
            builder.AddJsonFile(path: AppSettingsPath, optional: true, reloadOnChange: true);
            return builder;
        }
    }
}

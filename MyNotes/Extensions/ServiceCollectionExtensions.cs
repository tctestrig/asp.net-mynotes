﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MyNotesDatabase;
using MyNotesDatabase.Database;
using MyNotesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        private const string DefaulDbName = "Default";
        public static IServiceCollection AddInMemoryDatabase(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<MyNotesDbContext>(options =>
                         options.UseInMemoryDatabase(config.GetDatabaseName() ?? DefaulDbName));
            return services;
        }

        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository, Repository>();
            return services;
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using MyNotes.Utility;
using MyNotesDTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyNotes.ModelBinder
{
    public class ModelBinderBase<TModel> : IModelBinder where TModel : new()
    {
        public virtual Task BindModelAsync(ModelBindingContext bindingContext)
        {
            Guard.ThrowOnArgumentNull(bindingContext);
            var instance = new TModel();
            var properties = typeof(TModel).GetProperties()
                .Where(p => p.HasPublicSetter()).ToList();
            properties.ForEach(property =>
            {
                var propertyType = property.PropertyType;
                var propertyName = property.Name;
                var rawValue = bindingContext.ValueProvider.GetValue(propertyName).FirstValue ??
                                   GetDefault(propertyType)?.ToString();
                object convertedValue = null;
                if (rawValue != null)
                {
                    var converter = TypeDescriptor.GetConverter(propertyType);
                    convertedValue = converter.ConvertFrom(rawValue);
                }
                typeof(TModel).GetProperty(propertyName).SetValue(instance, convertedValue);
            });
            bindingContext.Result = ModelBindingResult.Success(instance);
            return Task.CompletedTask;
        }

        private static object GetDefault(Type type)
        {
            return type.IsValueType ?
                Activator.CreateInstance(type) : null;
        }
    }
}

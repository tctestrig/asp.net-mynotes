﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using MyNotes.Utility;
using MyNotesDTOs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MyNotes.ModelBinder
{
    public class NoteModelBinder : ModelBinderBase<Note>
    {
        public override Task BindModelAsync(ModelBindingContext bindingContext)
        {
            return base.BindModelAsync(bindingContext);
        }
    }
}

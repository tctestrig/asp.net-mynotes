﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;
using MyNotes.Utility;
using MyNotesDTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyNotes.ModelBinder
{
    public class ModelBinderProvider<TModelBinder, TModel> : IModelBinderProvider 
        where TModelBinder : IModelBinder 
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            Guard.ThrowOnArgumentNull(context);
            if (context.Metadata.ModelType == typeof(TModel))
            {
                return new BinderTypeModelBinder(typeof(TModelBinder));
            }
            return null;
        }
    }
}

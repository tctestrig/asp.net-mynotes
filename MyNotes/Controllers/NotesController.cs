﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyNotesDTOs;
using MyNotesInterfaces;
using MyNotes.Models;
using MyNotes.Extensions;
using MyNotes.Attributes;
using MyNotesDTOs.Enum;

namespace MyNotes.Controllers
{
    public class NotesController : Controller
    {
        private readonly IRepository _repository;
        private const string NotesViewModelId = "NotesViewModel";

        public NotesController(IRepository repository)
        {
            _repository = repository;
        }

        // GET: Notes
        public async Task<IActionResult> Index()
        {
            var viewModel = GetSessionViewModel();
            if(viewModel == null)
            {
                viewModel = new NotesViewModel(_repository);
            }
            await viewModel.InitializeViewModel();
            await viewModel.ShowFinishedNotes();
            SetSessionViewModel(viewModel);

            return View(viewModel);
        }

        // GET: Notes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Notes/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([BindNote]Note note)
        {
            note.CreationDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                await _repository.AddNoteAsync(note);
                await _repository.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(note);
        }

        // GET: Notes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _repository.GetNoteByIdAsync((int)id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // POST: Notes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [BindNote]Note note)
        {
            if (id != note.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _repository.UpdateNote(note);
                    await _repository.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoteExists(note.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _repository.GetNoteByIdAsync((int)id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var note = await _repository.GetNoteByIdAsync((int)id);
            _repository.DeleteNote(note);
            await _repository.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NoteExists(int id)
        {
            return _repository.NoteExists(id);
        }

        #region AJAX Methods
        [ResponseCache(NoStore =true, Duration =0)]
        public async Task<ActionResult> ShowFinished()
        {
            var viewModel = GetSessionViewModel();
            await viewModel.ShowFinishedNotes(toggleShowFinishedState: true);
            SetSessionViewModel(viewModel);
            return PartialView("Index", viewModel);
        }

        [ResponseCache(NoStore = true, Duration = 0)]
        public ActionResult SortByFinishDate()
        {
            var viewModel = GetSessionViewModel();
            viewModel.SortNotes(SortType.FinishDate, toggleSortDirection: true);
            SetSessionViewModel(viewModel);
            return PartialView("Index", viewModel);
        }

        [ResponseCache(NoStore = true, Duration = 0)]
        public ActionResult SortByCreationDate()
        {
            var viewModel = GetSessionViewModel();
            viewModel.SortNotes(SortType.CreatedDate, toggleSortDirection: true);
            SetSessionViewModel(viewModel);
            return PartialView("Index", viewModel);
        }

        [ResponseCache(NoStore = true, Duration = 0)]
        public ActionResult SortByImportance()
        {
            var viewModel = GetSessionViewModel();
            viewModel.SortNotes(SortType.Importance, toggleSortDirection: true);
            SetSessionViewModel(viewModel);
            return PartialView("Index", viewModel);
        }

        [ResponseCache(NoStore = true, Duration = 0)]
        public ActionResult ChangeTheme(Theme theme)
        {
            var viewModel = GetSessionViewModel();
            SetSessionViewModel(viewModel);
            var cookie = Request.Cookies["theme"];
            Response.Cookies.Append("theme", Enum.GetName(typeof(Theme), theme), new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTime.Now.AddDays(10)});
            return PartialView("Index", viewModel);
        }
        #endregion

        #region helper methods
        private NotesViewModel GetSessionViewModel()
        {
            var viewModel = HttpContext.Session.GetObjectFromJson<NotesViewModel>(NotesViewModelId);
            viewModel?.SetRepository(_repository);
            return viewModel;
        }

        private void SetSessionViewModel(NotesViewModel viewModel)
        {
            HttpContext.Session.SetObjectAsJson(NotesViewModelId, viewModel);
        }
        #endregion
    }
}

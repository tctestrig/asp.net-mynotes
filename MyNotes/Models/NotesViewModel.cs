﻿using MyNotesDTOs;
using MyNotesDTOs.Enum;
using MyNotesInterfaces;
using System.Threading.Tasks;

namespace MyNotes.Models
{
    public class NotesViewModel
    {
        private IRepository _repository;

        public NotesList NotesInScope { get; set; }
        public Note SelectedNote { get; set; }

        public SortType SortType { get; set; }
        public SortDirection SortDirection { get; set; }

        public bool ShowFinished { get; set; }


        public NotesViewModel(IRepository repository)
        {
            _repository = repository;
        }

        public void SetRepository(IRepository repository)
        {
            _repository = repository;
        }

        public void SortNotes(SortType sortType, bool toggleSortDirection = false)
        {
            if (toggleSortDirection && SortType == sortType)
            {
                SortDirection = SortDirection.ToggleDirection();
            }
            else
            {
                SortType = sortType;
            }
            
            switch (sortType)
            {
                case SortType.FinishDate:
                    NotesInScope = NotesInScope.SortNotesByFinishDate(SortDirection);
                    break;
                case SortType.CreatedDate:
                    NotesInScope = NotesInScope.SortNotesByCreationDate(SortDirection);
                    break;
                case SortType.Importance:
                    NotesInScope = NotesInScope.SortNotesByImportance(SortDirection);
                    break;
                default:
                    break;
            }
        }

        public async Task ShowFinishedNotes(bool toggleShowFinishedState = false)
        {
            if(toggleShowFinishedState) ShowFinished = !ShowFinished; // Toggle
            if (ShowFinished)
            {
                NotesInScope = await _repository.GetAllNotesAsync();
            }
            else
            {
                NotesInScope = (await _repository.GetAllNotesAsync()).GetUnfinishedNotes();
            }
            SortNotes(SortType);
        }

        public async Task InitializeViewModel()
        {
            TryInitStates();
            NotesInScope = await _repository.GetAllNotesAsync();
            SortByCurrentSortType();
        }

        private void SortByCurrentSortType()
        {
            SortNotes(SortType);
        }

        private void TryInitStates()
        {
            if(SortType == SortType.Undefined) // First time init
            {
                ShowFinished = true;
                SortType = SortType.CreatedDate;
                SortDirection = SortDirection.Ascending;
            }
        }
    }
}
﻿using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyNotes.Controllers;
using MyNotesDTOs;
using MyNotesInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace MyNotes.Test
{
    [TestClass]
    public class NotesControllerTest : MyNotesTestBase
    {
        private NotesController _controller;

        [TestInitialize]
        public void InitTest()
        {
            _controller = new NotesController(Repository);
        }

        [TestMethod]
        public async Task Delete_PassInvalidId_ReturnsNotFound()
        {
            // Arrange
            var expectedResultCode = new NotFoundResult();

            // Act
            var actualResultCode = await _controller.Delete(default(int));

            // Assert
            actualResultCode.Should().BeEquivalentTo(expectedResultCode);
        }

        [TestMethod]
        public async Task Edit_PassInvalidId_ReturnsNotFound()
        {
            // Arrange
            var expectedResultCode = new NotFoundResult();

            // Act
            var actualResultCode = await _controller.Edit(default(int));

            // Assert
            actualResultCode.Should().BeEquivalentTo(expectedResultCode);
        }

        [TestMethod]
        public async Task Edit_UpdateExistingNote_NoteUpdatedCorrectly()
        {
            // Arrange
            const int expectedImportance = 2;
            const bool expectedIsFinished = true;
            const string expectedTitle = "SomeTitle";
            const string expectedText = "SomeText";
            var id = default(int);

            await _controller.Create(new Note());
            id = (await Repository.GetAllNotesAsync()).First().Id;

            // Act
            var noteToEdit = await Repository.GetNoteByIdAsync(id);
            noteToEdit.Importance = expectedImportance;
            noteToEdit.IsFinished = expectedIsFinished;
            noteToEdit.Title = expectedTitle;
            noteToEdit.Text = expectedText;
            await _controller.Edit(id, noteToEdit);

            // Assert
            var noteToTest = await Repository.GetNoteByIdAsync(id);
            noteToTest.Importance.Should().Be(expectedImportance);
            noteToTest.IsFinished.Should().Be(expectedIsFinished);
            noteToTest.Title.Should().Be(expectedTitle);
            noteToTest.Text.Should().Be(expectedText);
        }
    }
}

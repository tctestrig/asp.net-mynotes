﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyNotesInterfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyNotes.Test
{
    [TestClass]
    public abstract class MyNotesTestBase : IDisposable
    {
        public IServiceProvider Services { get; }
        public IRepository Repository { get; }

        public MyNotesTestBase()
        {
            Services = new ServiceCollection()
                .AddInMemoryDatabase(new ConfigurationBuilder()
                        .AddConfigFiles()
                        .Build())
                .AddRepository()
                .BuildServiceProvider();
            Repository = Services.CreateScope()
                .ServiceProvider.GetRequiredService<IRepository>();
        }

        public void Dispose()
        {
            Repository.Dispose();
        }
    }
}
